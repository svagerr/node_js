import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();


const button_one = document.getElementById('button_one');
button_one.addEventListener('click', ()=>{
    console.log('+');
});
