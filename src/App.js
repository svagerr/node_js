import React from 'react';
import Page1 from './components/Page1/Page1.jsx';
import Page2 from './components/Page2/Page2.jsx';
import Page3 from './components/Page3/Page3.jsx';
import Page4 from './components/Page4/Page4.jsx';
import Header from './components/Header/Header.jsx';
import {BrowserRouter,Route} from 'react-router-dom';
import './App.css';

const App = ()=> {
return (
    <BrowserRouter>
        <div className="app-wrapper">
            <Header />
            <div className="content">
                <Route path='/page1' render= { ()=> <Page1 /> }/>
                <Route path='/page2' render= { ()=> <Page2 /> }/>
                <Route path='/page3' render= { ()=> <Page3 /> }/>
                <Route path='/page4' render= { ()=> <Page4 /> }/>
            </div>
        </div>
    </BrowserRouter>
    );
}

export default App;