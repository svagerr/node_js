import React from 'react';
import s from './Page2.module.css';

export default class Timer extends React.Component {
    
    constructor() {
        super();
        this.state = {
            array: ['http://w3.org.ua/wp-content/uploads/2017/01/HTML5.jpg', 'https://horoshiy-otzyv.ru/wp-content/uploads/2018/04/css.png', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQeQRo01HuDkO7gINtxyFg4gcLA80YDv3b9SXEoOoBxjbIB1mpT', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_JPwWqY-nUBWC3DR0BrVb-88XUM6AGn58r8lB4qx0bhgR_aV78Q'],
            stringURL: 'http://w3.org.ua/wp-content/uploads/2017/01/HTML5.jpg',
            number: 0,
        };
        this.startTimer();
    }

    nextImg = () => {        
        this.setState((prevState) => {
            if(this.state.number === 3){
                return {
                    number: prevState.number = -1,
                }
            }
            return {
                    number: prevState.number + 1,
                    stringURL: prevState.stringURL = this.state.array[this.state.number + 1],
            }
        });
    };

    prevImg = () => {
        this.setState((prevState) => {
            if(this.state.number === 0){
                return {
                    number: prevState.number = 0,
                }
            }
            return {
                    number: prevState.number - 1,
                    stringURL: prevState.stringURL = this.state.array[this.state.number - 1],
            }
        });
    };

    startTimer = () => {
        this.timer1 = setInterval(this.nextImg, 1000);   
    }
    
    thisImg = (number) => {        
        this.setState((prevState) => {
            return {
                    number: prevState.number = number,
                    stringURL: prevState.stringURL = this.state.array[this.state.number + 1],
            }
        });
    };
    
    render() {
        return(
        <div className={s.content}>
            <button className={s.btn} onClick={this.prevImg}>◀</button>
            <div className={s.contents}>
                <img src={this.state.stringURL} />
                <div className={s.btni}>
                    <button className={s.btnImg} onClick={()=>{this.thisImg(-1)}}>1</button>
                    <button className={s.btnImg} onClick={()=>{this.thisImg(0)}}>2</button>
                    <button className={s.btnImg} onClick={()=>{this.thisImg(1)}}>3</button>
                    <button className={s.btnImg} onClick={()=>{this.thisImg(2)}}>4</button> 
                </div>
            </div>
            <button className={s.btn} onClick={this.nextImg}>▶</button>
        </div>
        );
    }

}