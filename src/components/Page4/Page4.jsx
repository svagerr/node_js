import React from 'react';
import s from './Page4.module.css';
import {NavLink} from "react-router-dom";

const DialogItem = (props)=>{
    const path = "/page4/" + props.id;
    return(
        <div className={s.dialog}>
            <NavLink to={path} className={s.a}>{ props.name }</NavLink>
        </div>
    );
};
const MassegePersone = (props)=>{
    return (
        <div>{props.massege}</div>
    );
}

const Page4 = (props)=>{
    
    let dialogsArray = [{id: 1, name: "Dima"}, {id: 2, name: "Nastia"}, {id: 3, name: "Mother"}, {id: 4, name: "Sister"}, {id: 5, name: "Nastia"}];
    
    let newArray = dialogsArray.map( dialog => {
        return <DialogItem name={dialog.name} id={dialog.id}/>
    });
    
    return(
        <div className={s.dialogs}>
           <div className={ s.dialogsItems }>
                { newArray }
           </div>
           <div className={s.messages}>
               <div className={s.messages__persone}>
                   <MassegePersone massege="Hi" />
                   <MassegePersone massege="My" />
                   <MassegePersone massege="Name" />
                   <MassegePersone massege="Dima" />
               </div>
           </div>
        </div>
    )
};

export default Page4;