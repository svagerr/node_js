import React from 'react';
import s from './Header.module.css';
import {NavLink} from "react-router-dom";


const Header = (props)=> {
return (
    <div className={s.header}>
        <div className = {s.item}>
          <NavLink to ="/page1" activeClassName={s.active} className={s.a} id="button_one">Page 1</NavLink> 
       </div>
        <div className = {s.item}>
          <NavLink to ="/page2" activeClassName={s.active} className={s.a}>Page 2</NavLink> 
       </div>        
    </div>
    );
};

export default Header;