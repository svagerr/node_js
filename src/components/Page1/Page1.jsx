import React from 'react';
import s from './Page1.module.css';
import Logic from './logic.js';


export default class Timer extends React.Component {
    
    constructor() {
        super();
        this.state = {
            second: 0,
            minute: 0,
            strSecond: '0',
        };
    }
    
    startTimer = () => {
        this.timer1 = setInterval(this.onClickUpSecond, 1000);   
    }
    
    pauseTimer = () => {
        clearInterval(this.timer1);
    }
    
    resetTimer = () => {
        this.setState((prevState) => {
            return {
                second: prevState.second = 0,
                minute: prevState.minute = 0,
            }
        })
    }
    
    onClickUpSecond = () => {
        this.setState((prevState) => {
            if((this.state.second + 1) === 60){
                return {
                    minute: prevState.minute + 1,
                    second: prevState.second = 0,
                }
            } else {
                return {
                    second: prevState.second + 1,
                }    
            }
        });
    };
    
    render() {
        return(
        <div className={s.content}>
            <button onClick={this.startTimer} className={s.btn}>Start</button>
            <button onClick={this.pauseTimer} className={s.btn}>Pause</button>
            <button onClick={this.resetTimer} className={s.btn}>Reset</button>
            <div className={s.timer}>{`${this.state.minute}:${this.state.second}`}</div>
        </div>
        );
    }

}